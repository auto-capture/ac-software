#TITRE: Auto-capture - Envoyer vos screenshots directement sur votre serveur
#SOUS-TITRE: Gagner du temps lors du partage de vos captures d'écrans

Bonjour à tous, je viens aujourd'hui vous présentez un projet que 
j'avais envie de réaliser depuis un moment.

[[information]]
| Il est susceptible que ce post soit modifié au fil des mises à jour du logiciel

[[attention]]
| Le projet n'a pas encore de nom officiel, le nom de développement actuel est donc **Auto-Capture**. J'avais aussi
| pensé à **ScreenUp**, acronyme de *Screenshot Upload*.  
| Si vous avez des idées pour un nom, je serais ravi de les entendres :D

#Genèse

Lorsque je communique sur des forums ou autres, je dois souvent joindre des captures d'écrans.

En principe, il est nécessaire de suivre les étapes suivantes pour arriver à nos fins :

- Prendre la capture avec un outil de capture
- La publié en ligne (Sur un serveur externe, sur un serveur OwnCloud, etc.)
- Et finallement ajouter le lien là ou nous souhaitons afficher l'image

Ces opérations peuvent vite prendre du temps lorsque nous devons ajouter plusieurs capture d'écran.

C'est pour cette raison que j'ai développé un logiciel permettant de prendre une capture,
et de l'envoyer sur un serveur de façon automatique.

#Fonctionnement

##Logiciel

Le logiciel, écrit en Java, va nous permettre de créer une capture et d'envoyer notre image sur le serveur.

![Le logiciel sur un thème clair](https://owncloud.winxaito.com/index.php/s/5TAFuoTYawEFiqh/download)

Une fois la capture effectuée, si la coche `Envoyer automatiquement l'image` est activé, alors l'image sera
uploadé directement sur le serveur web. Il nous suffira ensuite de cliquer sur le lien `Copié` qui ajoutera
le lien de l'image dans le presse papier.


##Serveur web

Passons maintenant au serveur, il y aura donc un serveur principal où tout le monde pourra y uploader ses images,
mais pour ce faire, il sera nécessaire de s'inscrire sur le site et une limite d'espace disque sera appliqué.

Mais l'un des grands avantages d'auto-capture, réside dans le fait que le logiciel, ainsi que l'API web sont
open-source. L'API a été conçu pour être simple à être installé sur un serveur web.

Donc pour les personnes possédant un VPS ou un hébergement web pourront installé cette API, les seules nécessités
sont d'avoir `PHP 7` ainsi qu'un serveur `MySQL` d'installés. Il sera ensuite possible pour l'administrateur de gérer 
plusieurs paramètres.

Voici une liste non exhaustive des paramètres que l'administrateur peut gérer:

- La possibilité pour d'autres membres de s'inscrire (Ceci est désactivable, et alors seul l'administrateur pourra
enregistrer des membres).
- La possibilité de gérer un quotas général pour le serveur.
- La possibilité de gérer les quotas individuellement pour chaque utilisateur.
- Possibilité de configurer un serveur Sentry pour capturer les erreurs
- Ainsi que bien d'autres options.

###Zestedesavoir

J'aimerais également voir s'il y a une possibilité d'uploader directement sur une galerie de son compte
sur Zestedesavoir (Comme ZestWriter le fait de mémoire). Ce n'est pas ma priorité pour le moment, mais
ça reste une feature intéressante.

#Open-Source

Le projet sera totalement Open-Source[^opensource] une fois la première version Bêta déployé, ou lorsque j'aurai trouver un
nom officiel pour le projet. Il sera possible à tous les membres de proposer des PRs, de forker le projet 
et de le modifier à leurs guise.

La seule restriction que j'impose est l'interdiction de revendre le logiciel, ou de se faire de l'argent avec l'API.

[[question]]
| Au passage, que me conseillez-vous comme licence selon les indications données ci-dessus ?

#Avancement

Actuellement, au niveau du logiciel, il est possible de faire des captures, de les envoyer sur le serveur défini,
de [vérouiller]() et [dévérouiller]() l'écran. Au niveau de développement il reste à gérer les options pour utiliser
un autre serveur, l'authentification, tout ce qui touche aux galeries de Zestedesavoir et les options pour le thème et
la langue.

Mais une première version du thème `dark` est déjà défini:

![Thème dark](https://owncloud.winxaito.com/index.php/s/wM2ZpMJNuyY09ZM/download)

J'aimerais encore créer des autres thèmes se rapprochant plus des OS (Windows, Linux, Mac, etc.).

##Paramètres

![Paramètres](https://owncloud.winxaito.com/index.php/s/jnecq0M9OKMhROS/download)


#Conclusion

Si vous avez des remarques, question, suggestions et j'en passe, n'hésitez pas à m'en faire part.

Je ne sais pas si vous pensez que cet outil peut avoir une utilité pour vous, mais je le développe avant tout
pour moi et ce projet m'apporte certaine connaissance que je n'avais pas (Tel que l'upload de fichier, l'utilisation
du presse papier, etc.)

Je vous remercie, WinXaito ;)





[^opensource]: Je ne sais pas si l'on peut considérer totalement le projet comme Open-Source étant donné que
je n'autorise les utilisateurs à se faire de l'argent en revandant le logiciel ou en proposant des avantages
sur leur serveur.

*[auto-capture]: Nom temporaire  
*[uploader]: Télécharger sur le serveur  
*[uploadé]: Télécharger sur le serveur  