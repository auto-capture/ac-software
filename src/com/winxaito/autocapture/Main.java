package com.winxaito.autocapture;

import com.winxaito.autocapture.controllers.auth.LoginController;
import com.winxaito.autocapture.controllers.capture.CaptureController;
import com.winxaito.autocapture.controllers.trayicon.TrayIcon;
import com.winxaito.autocapture.model.utils.SettingsManager;
import com.winxaito.autocapture.model.utils.SettingsModel;
import com.winxaito.autocapture.model.utils.ShortcutListener;
import javafx.application.Application;
import javafx.scene.input.Clipboard;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class Main extends Application{
    private static Main main;
    private static Logger logger = LogManager.getLogger();

    public static final String URL_DOMAIN_UI = "https://pic.winxaito.com";
    public static final String URL_DOMAIN_DOC = "https://pic.winxaito.com/doc";
    public static final String URL_DOMAIN_API = "https://pic.winxaito.com/api";

    public static final String CAPTURE_PATH = SettingsManager.getDataDirectory() + "/wxpic/captures/";
    public static Clipboard clipboard;
    public static Robot robot;
    public static SettingsModel settings;

    @Override
    public void start(Stage primaryStage) throws Exception{
        clipboard = Clipboard.getSystemClipboard();
        CaptureController.lauch(false);
        TrayIcon.init();

        if(settings.getServer_username().equals("") || settings.getServer_password().equals("")){
            LoginController.lauch();
        }else{
            CaptureController.showWindow();
            CaptureController.initLogin();
            ShortcutListener.init();
            TrayIcon.show();
        }
    }

    public static void main(String[] args) {
        logger.debug("Starting programm...");

        main = new Main();
        settings = SettingsManager.loadSettings();

        try{
            robot = new Robot();
        }catch(AWTException e){
            logger.error(e);
        }

        launch(args);
        ShortcutListener.exit();
        logger.debug("Ending program...");
    }

    public static Main getApp(){
        return main;
    }
}
