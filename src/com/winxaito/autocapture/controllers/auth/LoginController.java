package com.winxaito.autocapture.controllers.auth;

import com.winxaito.autocapture.Main;
import com.winxaito.autocapture.controllers.capture.CaptureController;
import com.winxaito.autocapture.controllers.trayicon.TrayIcon;
import com.winxaito.autocapture.controllers.utils.SettingsController;
import com.winxaito.autocapture.model.network.ApiRequest;
import com.winxaito.autocapture.model.network.ApiUploadXml;
import com.winxaito.autocapture.model.network.LoginModel;
import com.winxaito.autocapture.model.utils.SettingsManager;
import com.winxaito.autocapture.model.utils.SettingsModel;
import com.winxaito.autocapture.model.utils.ShortcutListener;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class LoginController{
    private static Logger logger = LogManager.getLogger();

    private static Stage window;
    private double xDragOffset;
    private double yDragOffset;

    //FXML variables
    @FXML private HBox drag;
    @FXML private Label data_server;
    @FXML private TextField username;
    @FXML private PasswordField password;
    @FXML private Label error;
    @FXML private ProgressIndicator loader;

    public static void lauch() throws IOException{
        logger.debug("Lauch loginOfficialServer stage");

        window = new Stage();
        Parent root = FXMLLoader.load(LoginController.class.getResource("/fxml/auth/LoginView.fxml"));
        Scene scene = new Scene(root, 500, 270);
        scene.getStylesheets().addAll("/css/application.css", Main.settings.getGeneral_theme_path());
        window.getIcons().setAll(new Image("/images/logo/logo_32x32.png"));
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.setTitle("Connexion");

        window.show();
    }

    public static Stage getWindow(){
        return window;
    }

    //FXML Functions

    @FXML
    public void initialize(){
        loader.setVisible(false);
        error.setText("");
        error.setVisible(false);

        if(Main.settings.getServer_type() == SettingsModel.ServerType.OFFICIAL)
            data_server.setText(Main.URL_DOMAIN_API);
        else if(Main.settings.getServer_type() == SettingsModel.ServerType.UNOFFICIAL)
            data_server.setText(Main.settings.getServer_unofficial_protocol() + "://" +
                    Main.settings.getServer_unofficial_host() + ":" + Main.settings.getServer_unofficial_port());
        else
            data_server.setText("Invalid server type");

        drag.setOnMousePressed(event -> {
            xDragOffset = window.getX() - event.getScreenX();
            yDragOffset = window.getY() - event.getScreenY();
        });
        drag.setOnMouseDragged(event -> {
            window.setX(event.getScreenX() + xDragOffset);
            window.setY(event.getScreenY() + yDragOffset);
        });
    }

    @FXML
    public void handleClose(){
        window.close();
    }

    @FXML
    public void handleIconify(){
        window.setIconified(true);
    }

    @FXML
    public void handleConnect(){
        hideError();

        if(username.getText().equals("") || password.getText().equals("")){
            error.setText("Merci de remplir tous les champs");
            showError();
            return;
        }

        showLoader();

        connect(username.getText(), password.getText(), Main.settings.getServer_unofficial_protocol(),
                Main.settings.getServer_unofficial_host(), Main.settings.getServer_unofficial_port());
    }

    @FXML
    public void handleRegister(){
        logger.debug("Open register");
        Main.getApp().getHostServices().showDocument(Main.URL_DOMAIN_UI + "/register");
    }

    @FXML
    public void handleHelp(){
        logger.debug("Open help");
        Main.getApp().getHostServices().showDocument(Main.URL_DOMAIN_DOC + "/auth/loginOfficialServer");
    }

    @FXML
    public void handleConfig(){
        logger.debug("Open config");

        try{
            SettingsController.launch();
        }catch(IOException e){
            logger.error(e);
            e.printStackTrace();
        }
    }

    private void connect(String username, String password, String protocol, String domain, int port){
        logger.debug("Try to connect");

        Task<Void> task = new Task<Void>(){
            @Override
            protected Void call() throws Exception{
                double timeStart = System.currentTimeMillis();

                ApiUploadXml uploadXml = LoginModel.login(username, password, protocol, domain, port);

                if(uploadXml != null){
                    logger.debug("Start check time for minimum 1s");
                    double time = System.currentTimeMillis() - timeStart;
                    while(time < 1000){
                        Thread.sleep(50);
                        time = System.currentTimeMillis() - timeStart;
                    }

                    logger.debug("Check if request has error");
                    if(uploadXml.hasError()){
                        Platform.runLater(() -> error.setText("Code d'erreur: " + uploadXml.getError().getCode() + "\n" + uploadXml.getError().getMessage()));
                        hideLoader();
                        showError();
                    }else{
                        logger.debug("Update username and password");
                        Main.settings.setServer_username(username);
                        Main.settings.setServer_password(password);
                        SettingsManager.update(Main.settings);
                        logger.debug("Config updated");

                        Platform.runLater(() -> {
                            window.close();
                            CaptureController.showWindow();
                            ShortcutListener.init();
                            TrayIcon.show();
                        });
                    }
                }else{
                    Platform.runLater(() -> error.setText("Une erreur est survenu, veuillez vous assurez que le serveur fonctionne correctement"));
                    hideLoader();
                    showError();
                }

                return null;
            }
        };

        new Thread(task).start();
    }

    private void showLoader(){
        Platform.runLater(() -> loader.setVisible(true));
    }

    private void hideLoader(){
        Platform.runLater(() -> loader.setVisible(false));
    }

    private void showError(){
        Platform.runLater(() -> error.setVisible(true));
    }

    private void hideError(){
        Platform.runLater(() -> error.setVisible(false));
    }
}
