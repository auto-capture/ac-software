package com.winxaito.autocapture.controllers.capture;

import com.winxaito.autocapture.Main;
import com.winxaito.autocapture.controllers.auth.LoginController;
import com.winxaito.autocapture.controllers.trayicon.TrayIcon;
import com.winxaito.autocapture.controllers.utils.SettingsController;
import com.winxaito.autocapture.model.capture.CaptureModel;
import com.winxaito.autocapture.model.network.ApiRequest;
import com.winxaito.autocapture.model.network.ApiUploadXml;
import com.winxaito.autocapture.model.network.LoginModel;
import com.winxaito.autocapture.model.utils.SettingsModel;
import com.winxaito.autocapture.model.utils.StatusModel;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class CaptureController{
    private static Logger logger = LogManager.getLogger();

    private double xDragOffset;
    private double yDragOffset;
    private static Stage window;

    private static FadeTransition fadeTransitionShow;
    private static FadeTransition fadeTransitionHide;

    private static boolean static_autoupload;
    private static boolean static_autosave;

    //FXML Variables
    @FXML private HBox drag;
    @FXML private Label status;

    @FXML private CheckBox setting_lock;
    @FXML private CheckBox setting_autoupload;
    @FXML private CheckBox setting_autosave;
    @FXML private Button setting_button_upload;
    @FXML private Button setting_button_save;

    @FXML private Label recent_upload_1;
    @FXML private Label recent_upload_2;
    @FXML private Label recent_upload_3;
    @FXML private Label recent_upload_4;
    @FXML private Label recent_upload_5;
    @FXML private Hyperlink copy_1;
    @FXML private Hyperlink copy_2;
    @FXML private Hyperlink copy_3;
    @FXML private Hyperlink copy_4;
    @FXML private Hyperlink copy_5;

    public static void lauch(boolean show) throws IOException{
        window = new Stage();
        Parent root = FXMLLoader.load(LoginController.class.getResource("/fxml/capture/CaptureView.fxml"));
        window.setTitle("Capture");
        Scene scene = new Scene(root, 450, 400);
        scene.getStylesheets().addAll("/css/application.css", Main.settings.getGeneral_theme_path());
        scene.setFill(Color.TRANSPARENT);
        window.getIcons().setAll(new Image("/images/logo/logo_32x32.png"));
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.setAlwaysOnTop(true);

        fadeTransitionShow = new FadeTransition(Duration.millis(800), root);
        fadeTransitionShow.setFromValue(0.0);
        fadeTransitionShow.setToValue(1.0);

        fadeTransitionHide = new FadeTransition(Duration.millis(500), root);
        fadeTransitionHide.setFromValue(1.0);
        fadeTransitionHide.setToValue(0.0);

        if(show)
            window.show();
    }

    public void addUploadLink(String link){
        recent_upload_5.setText(recent_upload_4.getText());
        recent_upload_4.setText(recent_upload_3.getText());
        recent_upload_3.setText(recent_upload_2.getText());
        recent_upload_2.setText(recent_upload_1.getText());
        recent_upload_1.setText(link);

        if(!recent_upload_1.getText().equals("")){
            recent_upload_1.setVisible(true);
            copy_1.setVisible(true);
        }
        if(!recent_upload_2.getText().equals("")){
            recent_upload_2.setVisible(true);
            copy_2.setVisible(true);
        }
        if(!recent_upload_3.getText().equals("")){
            recent_upload_3.setVisible(true);
            copy_3.setVisible(true);
        }
        if(!recent_upload_4.getText().equals("")){
            recent_upload_4.setVisible(true);
            copy_4.setVisible(true);
        }
        if(!recent_upload_5.getText().equals("")){
            recent_upload_5.setVisible(true);
            copy_5.setVisible(true);
        }
    }

    public static void hideWindow(){
        fadeTransitionHide.play();
    }

    public static void directHideWindow(){
        window.hide();
    }

    public static void showWindow(){
        fadeTransitionShow.play();
        window.show();
    }

    public static Stage getWindow(){
        return window;
    }

    public Label getStatus(){
        return status;
    }

    public static void startCapture(boolean lock){
        logger.debug("Starting capture (Lock: " + lock + ")");
        CaptureModel.startCapture(static_autosave, static_autoupload, lock);
    }

    public static void initLogin(){
        logger.debug("Init loginOfficialServer");
        StatusModel.setStatusMessage("Tentative de connexion");

        Task<Void> task = new Task<Void>(){
            @Override
            protected Void call() throws Exception{
                if(Main.settings.getServer_username().equals("") || Main.settings.getServer_password().equals("")){
                    logger.debug("Username or password in settings is empty");
                    hideWindow();
                    LoginController.lauch();
                }else{
                    ApiUploadXml uploadXml = LoginModel.login(Main.settings.getServer_username(), Main.settings.getServer_password(),
                            Main.settings.getServer_unofficial_protocol(),
                            Main.settings.getServer_unofficial_host(), Main.settings.getServer_unofficial_port());

                    if(uploadXml == null || uploadXml.hasError()){
                        if(uploadXml == null)
                            logger.debug("Inaccessible server");
                        else if(uploadXml.hasError())
                            logger.debug("Username or password is incorrect");

                        Platform.runLater(() -> {
                            window.close();
                            try{
                                TrayIcon.hide();
                                LoginController.lauch();
                            }catch(IOException e){
                                logger.error(e);
                            }
                            StatusModel.clearStatusMessage();
                        });
                    }else{
                        Platform.runLater(() -> StatusModel.setStatusMessage("Connexion réussi"));
                        logger.debug("Login successful");
                    }
                }

                return null;
            }
        };

        new Thread(task).start();
    }

    //FXML Functions

    @FXML
    private void initialize(){
        StatusModel.init(this);

        setting_autoupload.setSelected(true);
        setting_autosave.setSelected(true);
        setting_button_upload.setVisible(false);
        setting_button_save.setVisible(false);

        recent_upload_1.setText("");
        recent_upload_2.setText("");
        recent_upload_3.setText("");
        recent_upload_4.setText("");
        recent_upload_5.setText("");

        recent_upload_1.setVisible(false);
        recent_upload_2.setVisible(false);
        recent_upload_3.setVisible(false);
        recent_upload_4.setVisible(false);
        recent_upload_5.setVisible(false);

        copy_1.setVisible(false);
        copy_2.setVisible(false);
        copy_3.setVisible(false);
        copy_4.setVisible(false);
        copy_5.setVisible(false);

        static_autoupload = setting_autoupload.isSelected();
        static_autosave = setting_autosave.isSelected();

        setting_lock.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                logger.info("Enable capture lock");
                StatusModel.setStatusMessage("Activation de la fixation de l'écran");
            }else{
                logger.info("Disable capture lock");
                StatusModel.setStatusMessage("Désactivation de la fixation de l'écran");
            }
        });
        setting_autoupload.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                logger.info("Enable auto upload");
                StatusModel.setStatusMessage("Activation de l'envoie automatique");
                static_autoupload = true;
                static_autoupload = true;
            }else{
                logger.info("Disable auto upload");
                StatusModel.setStatusMessage("Désactivation de l'envoie automatique");
            }
        });
        setting_autosave.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                logger.info("Enable auto save");
                StatusModel.setStatusMessage("Activation de la sauvegarde automatique");
                static_autosave = true;
            }else{
                logger.info("Disable auto save");
                StatusModel.setStatusMessage("Désactivation de la sauvegarde automatique");
                static_autosave = false;
            }
        });

        drag.setOnMousePressed(event -> {
            xDragOffset = window.getX() - event.getScreenX();
            yDragOffset = window.getY() - event.getScreenY();
        });
        drag.setOnMouseDragged(event -> {
            window.setX(event.getScreenX() + xDragOffset);
            window.setY(event.getScreenY() + yDragOffset);
        });
    }

    @FXML
    public void handleClose(){
        window.close();
    }

    @FXML
    public void handleIconify(){
        window.setIconified(true);
    }

    @FXML
    public void handleCapture(){
        startCapture(setting_lock.isSelected());
    }

    @FXML
    public void handleSettings(){
        logger.debug("Open settings dialog");
        status.setText("Ouvertures des options");

        try{
            CaptureController.hideWindow();
            SettingsController.launch();
            CaptureController.showWindow();
        }catch(IOException e){
            logger.error(e);
        }
    }

    @FXML
    public void handleHelp(){
        logger.debug("Open capture help");
        status.setText("Ouverture de l'aide");
        Main.getApp().getHostServices().showDocument(Main.URL_DOMAIN_DOC + "/screenshots");
    }

    @FXML
    public void handleUpload(){
        logger.debug("Upload (NOT IMPLEMENTED)");
    }

    @FXML
    public void handleSave(){
        logger.debug("Save (NOT IMPLEMENTED)");
    }

    @FXML
    public void handleCopy1(){
        logger.debug("Copy link 1");

        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(recent_upload_1.getText());
        clipboardContent.putUrl(recent_upload_1.getText());
        Main.clipboard.setContent(clipboardContent);

        logger.debug("Link copied");
        StatusModel.setStatusMessage("Lien copié");
    }

    @FXML
    public void handleCopy2(){
        logger.debug("Copy link 2");

        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(recent_upload_2.getText());
        clipboardContent.putUrl(recent_upload_2.getText());
        Main.clipboard.setContent(clipboardContent);

        logger.debug("Link copied");
        StatusModel.setStatusMessage("Lien copié");
    }

    @FXML
    public void handleCopy3(){
        logger.debug("Copy link 3");

        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(recent_upload_3.getText());
        clipboardContent.putUrl(recent_upload_3.getText());
        Main.clipboard.setContent(clipboardContent);

        logger.debug("Link copied");
        StatusModel.setStatusMessage("Lien copié");
    }

    @FXML
    public void handleCopy4(){
        logger.debug("Copy link 4");

        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(recent_upload_4.getText());
        clipboardContent.putUrl(recent_upload_4.getText());
        Main.clipboard.setContent(clipboardContent);

        logger.debug("Link copied");
        StatusModel.setStatusMessage("Lien copié");
    }

    @FXML
    public void handleCopy5(){
        logger.debug("Copy link 5");

        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(recent_upload_5.getText());
        clipboardContent.putUrl(recent_upload_5.getText());
        Main.clipboard.setContent(clipboardContent);

        logger.debug("Link copied");
        StatusModel.setStatusMessage("Lien copié");
    }
}
