package com.winxaito.autocapture.controllers.trayicon;

import com.winxaito.autocapture.Main;
import com.winxaito.autocapture.controllers.capture.CaptureController;
import javafx.application.Platform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;


/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class TrayIcon{
    private static Logger logger = LogManager.getLogger();

    private static SystemTray tray;
    private static java.awt.TrayIcon trayIcon;

    private static boolean initialized = false;
    private static boolean show = false;

    public static void init(){
        if(!initialized){
            initialized = true;
            Platform.setImplicitExit(false);
            addAppToTray();
        }else{
            show();
        }
    }

    public static void show(){
        if(!initialized)
            init();

        if(!show){
            try{
                logger.debug("Show tray icon");
                tray.add(trayIcon);
                show = true;
            }catch(AWTException e){
                logger.error(e);
            }
        }
    }

    public static void hide(){
        if(show){
            logger.debug("Hide tray icon");
            tray.remove(trayIcon);
            show = false;
        }
    }

    /**
     * Sets up a system tray icon for the application.
     */
    private static void addAppToTray() {
        logger.debug("Add app to tray");

        try {
            // ensure awt toolkit is initialized.
            java.awt.Toolkit.getDefaultToolkit();

            // app requires system tray support, just exit if there is no support.
            if (!java.awt.SystemTray.isSupported()) {
                logger.error("No system tray support, application exiting");
                //Platform.exit();
                return;
            }

            // set up a system tray icon.
            tray = SystemTray.getSystemTray();
            InputStream icon = Main.class.getResourceAsStream("/images/logo/logo_16x16.png");
            java.awt.Image image = ImageIO.read(icon);
            trayIcon = new java.awt.TrayIcon(image);

            // if the user double-clicks on the tray icon, show the main app stage.
            trayIcon.addActionListener(event -> Platform.runLater(CaptureController::showWindow));
            addItems();

            // add the application tray icon to the system tray.
        } catch (IOException e) {
            logger.error("Unable to load system tray", e);
        }
    }

    private static void addItems(){
        // if the user selects the default menu item (which includes the app name),
        // show the main app stage.
        java.awt.MenuItem showItem = new java.awt.MenuItem("Masquer la fenetetre");

        ActionListener alShow = e -> Platform.runLater(CaptureController::showWindow);
        ActionListener alHide = e -> Platform.runLater(() -> CaptureController.getWindow().close());

        showItem.addActionListener(alHide);

        CaptureController.getWindow().setOnHidden(event -> {
            showItem.setLabel("Montrer la fenetre");
            showItem.removeActionListener(alHide);
            showItem.addActionListener(alShow);
        });
        CaptureController.getWindow().setOnShown(event -> {
            showItem.setLabel("Masquer la fenetre");
            showItem.removeActionListener(alShow);
            showItem.addActionListener(alHide);
        });


        // to really exit the application, the user must go to the system tray icon
        // and select the exit option, this will shutdown JavaFX and remove the
        // tray icon (removing the tray icon will also shut down AWT).
        java.awt.MenuItem exitItem = new java.awt.MenuItem("Quitter");
        exitItem.addActionListener(event -> {
            Platform.exit();
            tray.remove(trayIcon);
        });

        // setup the popup menu for the application.
        final java.awt.PopupMenu popup = new java.awt.PopupMenu();
        popup.add(showItem);
        popup.addSeparator();
        popup.add(exitItem);
        trayIcon.setPopupMenu(popup);
    }
}
