package com.winxaito.autocapture.controllers.utils;

import com.winxaito.autocapture.Main;
import com.winxaito.autocapture.controllers.auth.LoginController;
import com.winxaito.autocapture.controllers.capture.CaptureController;
import com.winxaito.autocapture.model.network.ApiRequest;
import com.winxaito.autocapture.model.network.ApiUploadXml;
import com.winxaito.autocapture.model.utils.SettingsManager;
import com.winxaito.autocapture.model.utils.SettingsModel;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class SettingsController{
    private static Logger logger = LogManager.getLogger();

    private double xDragOffset;
    private double yDragOffset;
    private static Stage window;

    //FXML Variables
    @FXML private HBox drag;

    @FXML private ComboBox<String> general_theme;
    @FXML private ComboBox<String> general_lang;
    @FXML private TextField general_savepath;

    @FXML private TextField shortcut_without_lock;
    @FXML private TextField shortcut_with_lock;
    @FXML private TextField shortcut_fullscreen;

    @FXML private TextField server_username;
    @FXML private TextField server_password;
    @FXML private ProgressIndicator loginLoader;
    @FXML private Circle loginStatus;

    @FXML private RadioButton server_type_official;
    @FXML private RadioButton server_type_unofficial;
    @FXML private RadioButton server_type_zds;
    @FXML private TextField server_unofficial_protocol;
    @FXML private TextField server_unofficial_host;
    @FXML private TextField server_unofficial_port;
    @FXML private TextField server_zds_gallery;

    @FXML private ToggleGroup type_server;
    @FXML private GridPane server_panel_unofficial;
    @FXML private GridPane server_panel_zds;


    public static void launch() throws IOException{
        window = new Stage();
        Parent root = FXMLLoader.load(LoginController.class.getResource("/fxml/utils/SettingsView.fxml"));
        window.setTitle("Options");
        Scene scene = new Scene(root, 500, 450);
        scene.getStylesheets().addAll("/css/application.css", Main.settings.getGeneral_theme_path());
        scene.setFill(Color.TRANSPARENT);
        window.getIcons().setAll(new Image("/images/logo/logo_32x32.png"));
        window.setScene(scene);
        window.initStyle(StageStyle.TRANSPARENT);
        window.initModality(Modality.APPLICATION_MODAL);
        window.showAndWait();

        window.setOnCloseRequest(event -> CaptureController.showWindow());
    }

    private void initFields(){
        loginLoader.setVisible(false);
        loginStatus.setFill(Color.web("#f8ad32"));

        for(SettingsModel.Theme theme : SettingsModel.Theme.values())
            general_theme.getItems().add(theme.text);
        general_theme.getSelectionModel().select(Main.settings.getGeneral_theme().text);

        for(SettingsModel.Lang lang : SettingsModel.Lang.values())
            general_lang.getItems().add(lang.text);
        general_lang.getSelectionModel().select(Main.settings.getGeneral_lang().text);

        server_username.setText(Main.settings.getServer_username());
        server_password.setText(Main.settings.getServer_password());
        server_unofficial_protocol.setText(Main.settings.getServer_unofficial_protocol());
        server_unofficial_host.setText(Main.settings.getServer_unofficial_host());
        server_unofficial_port.setText(Integer.toString(Main.settings.getServer_unofficial_port()));
        server_zds_gallery.setText(Main.settings.getServer_zds_gallery());

        if(Main.settings.getServer_type().configValue.equalsIgnoreCase("unofficial")){
            server_type_unofficial.setSelected(true);
            server_panel_unofficial.setVisible(true);
            server_panel_zds.setVisible(false);
        }else if(Main.settings.getServer_type().configValue.equalsIgnoreCase("zds")){
            server_type_zds.setSelected(true);
            server_panel_unofficial.setVisible(false);
            server_panel_zds.setVisible(true);
        }else{
            server_type_official.setSelected(true);
            server_panel_unofficial.setVisible(false);
            server_panel_zds.setVisible(false);
        }

        type_server.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if(((RadioButton)newValue).getText().equalsIgnoreCase("Non officiel")){
                server_panel_unofficial.setVisible(true);
                server_panel_zds.setVisible(false);
            }else if(((RadioButton)newValue).getText().equalsIgnoreCase("Zestedesavoir")){
                server_panel_unofficial.setVisible(false);
                server_panel_zds.setVisible(true);
            }else{
                server_panel_unofficial.setVisible(false);
                server_panel_zds.setVisible(false);
            }
        });

        server_unofficial_port.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                server_unofficial_port.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    //FXML Functions
    @FXML
    private void initialize(){
        initFields();

        //Drag
        drag.setOnMousePressed(event -> {
            xDragOffset = window.getX() - event.getScreenX();
            yDragOffset = window.getY() - event.getScreenY();
        });
        drag.setOnMouseDragged(event -> {
            window.setX(event.getScreenX() + xDragOffset);
            window.setY(event.getScreenY() + yDragOffset);
        });
    }

    @FXML
    public void handleClose(){
        window.close();
    }

    @FXML
    public void handleIconify(){
        window.setIconified(true);
    }

    @FXML
    public void handleCancel(){
        logger.debug("handleCancel");
        window.close();
    }

    @FXML
    public void handleSave(){
        logger.debug("handleSave");
        Main.settings.setGeneral_lang(general_lang.getSelectionModel().getSelectedItem());
        Main.settings.setGeneral_theme(general_theme.getSelectionModel().getSelectedItem());
        Main.settings.setServer_username(server_username.getText());
        Main.settings.setServer_password(server_password.getText());
        Main.settings.setServer_type(((RadioButton)type_server.getSelectedToggle()).getText());
        Main.settings.setServer_unofficial_protocol(server_unofficial_protocol.getText());
        Main.settings.setServer_unofficial_host(server_unofficial_host.getText());
        Main.settings.setServer_unofficial_port(server_unofficial_port.getText());

        SettingsManager.update(Main.settings);
        logger.debug("Settings updated");

        window.close();
    }

    @FXML
    public void handleCapturePathBrowse(){
        logger.debug("handleCapturePathBrowse");
    }

    @FXML
    public void handleCheckConnection(){
        logger.debug("handleCheckConnection");

        loginStatus.setFill(Color.web("#f8ad32"));
        loginLoader.setVisible(true);

        Task<Void> task = new Task<Void>(){
            @Override
            protected Void call() throws Exception{
                double timeStart = System.currentTimeMillis();

                try{
                    ApiUploadXml uploadXml;
                    if(server_type_official.isSelected()){
                        uploadXml = ApiRequest.loginOfficialServer(server_username.getText(), server_password.getText());
                    }else if(server_type_unofficial.isSelected()){
                        uploadXml = ApiRequest.loginUnofficialServer(server_username.getText(), server_password.getText(),
                                server_unofficial_protocol.getText(), server_unofficial_host.getText(),
                                Integer.parseInt(server_unofficial_port.getText()));
                    }else{
                        Platform.runLater(() -> {
                            Alert a = new Alert(Alert.AlertType.ERROR);
                            a.setTitle("Erreur");
                            a.setContentText("Seul <Serveur officiel> et <Serveur non officiel> sont diposnible");
                            a.showAndWait();
                        });
                        return null;
                    }


                    double time = System.currentTimeMillis() - timeStart;

                    while(time < 1000){
                        Thread.sleep(50);
                        time = System.currentTimeMillis() - timeStart;
                    }

                    if(uploadXml.hasError()){
                        Platform.runLater(() -> loginStatus.setFill(Color.DARKRED));

                        Platform.runLater(() -> {
                            Alert a = new Alert(Alert.AlertType.ERROR);
                            a.setTitle("Erreur");
                            a.setContentText("Code d'erreur: " + uploadXml.getError().getCode() + "\nMessage: " + uploadXml.getError().getMessage());
                            a.showAndWait();
                        });
                    }else{
                        Platform.runLater(() -> loginStatus.setFill(Color.DARKGREEN));
                    }
                }catch(IOException e){
                    logger.error(e);
                    Platform.runLater(() -> {
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle("Erreur");
                        a.setContentText("L'hôte ne semble pas être accessible, pour plus de détail, vérifier dans les logs de l'application");
                        a.showAndWait();
                    });
                    Platform.runLater(() -> loginStatus.setFill(Color.DARKRED));
                }finally{
                    Platform.runLater(() -> loginLoader.setVisible(false));
                }

                return null;
            }
        };

        new Thread(task).start();
    }

    @FXML
    public void handleDoc(){
        logger.debug("handleDocumentation");
        Main.getApp().getHostServices().showDocument(Main.URL_DOMAIN_DOC + "/settings");
    }
}
