package com.winxaito.autocapture.model.capture;

import com.winxaito.autocapture.Main;
import com.winxaito.autocapture.controllers.auth.LoginController;
import com.winxaito.autocapture.controllers.capture.CaptureController;
import com.winxaito.autocapture.model.network.ApiRequest;
import com.winxaito.autocapture.model.network.ApiUploadXml;
import com.winxaito.autocapture.model.utils.SettingsManager;
import com.winxaito.autocapture.model.utils.StatusModel;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.Robot;
import java.awt.Rectangle;
import java.awt.AWTException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class CaptureModel{
    private static Logger logger = LogManager.getLogger();

    private static Stage backgroundCaptureWindow;
    private static Stage backgroundImageLock;
    private static Canvas canvas;

    private static double originMouseX;
    private static double originMouseY;
    private static double mouseX;
    private static double mouseY;

    private static boolean save = false;
    private static boolean upload = false;

    private static boolean lock = false;

    /**
     * Start capture
     */
    public static void startCapture(boolean save, boolean upload, boolean lock){
        CaptureModel.lock = lock;
        CaptureController.directHideWindow();

        if(CaptureModel.lock)
            initBackgroundImageLock();

        initBackgroundCaptureWindow();

        CaptureModel.save = save;
        CaptureModel.upload = upload;

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            originMouseX = event.getX();
            originMouseY = event.getY();
        });

        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> {
            mouseX = event.getX();
            mouseY = event.getY();

            clearInterface(canvas.getGraphicsContext2D(), getX(), getY(), getWidth(), getHeight());
        });

        canvas.addEventHandler(MouseEvent.MOUSE_RELEASED, event -> {
            mouseX = event.getX();
            mouseY = event.getY();

            clearInterface(canvas.getGraphicsContext2D());

            if(getWidth() > 30 || getHeight() > 30){
                Platform.runLater(() -> {
                    try{
                        makeCapture();
                    }catch(AWTException | IOException e){
                        logger.error(e);
                    }
                });
            }else{
                logger.debug("Litle capture prevention");
            }
        });
    }

    /**
     * Make capture
     * @throws AWTException Exception
     * @throws IOException Exception
     */
    private static void makeCapture() throws AWTException, IOException{
        Rectangle captureRect = new Rectangle((int)getX() - 1920, (int)getY(), (int)getWidth(), (int)getHeight());

        //Hide capture window for the capture
        backgroundCaptureWindow.hide();

        //Capture screen with defined rectangle
        logger.debug("Starting screen with robot");
        BufferedImage image = Main.robot.createScreenCapture(captureRect);
        logger.debug("Ending capture with robot");

        backgroundCaptureWindow.hide();

        if(lock)
            backgroundImageLock.hide();

        //Show capture window after capture
        CaptureController.showWindow();

        Task<Void> task = new Task<Void>(){
            @Override
            protected Void call() throws Exception{
                //Write the image on the disk
                File tmpfile;
                tmpfile = new File(SettingsManager.getDataDirectory() + "/wxpic/temp/captures/" + System.currentTimeMillis() + ".png");

                if(!Files.exists(tmpfile.toPath()))
                    if(!tmpfile.mkdirs())
                        logger.error("Failed for create file in APPDATA <" + tmpfile.getName() + ">");

                ImageIO.write(image, "png", tmpfile);

                //TODO: BETA
                //** saveFile(tmpfile);
                uploadFile(tmpfile);
                removeTmpFile(tmpfile);

                return null;
            }
        };

        new Thread(task).start();
    }

    private static void initBackgroundImageLock(){
        Group root = new Group();

        backgroundImageLock = new Stage();
        backgroundImageLock.initStyle(StageStyle.UNDECORATED);
        backgroundImageLock.setAlwaysOnTop(true);

        Scene scene = new Scene(root, 5760, 1080);

        CaptureController.hideWindow();
        BufferedImage image = Main.robot.createScreenCapture(new Rectangle(-1920, 0, 5760, 1080));
        Image fxImage = SwingFXUtils.toFXImage(image, null);
        scene.setFill(new ImagePattern(fxImage));

        backgroundImageLock.setScene(scene);
        backgroundImageLock.setResizable(false);
        backgroundImageLock.setX(-1920);
        backgroundImageLock.setY(0);

        backgroundImageLock.show();
    }

    /**
     * Init background capture (Stage)
     */
    private static void initBackgroundCaptureWindow(){
        Group root = new Group();
        canvas = new Canvas(5760, 1080);
        root.getChildren().add(canvas);

        canvas.getGraphicsContext2D().setStroke(Color.DARKGRAY);
        canvas.getGraphicsContext2D().setLineWidth(5);
        clearInterface(canvas.getGraphicsContext2D());

        backgroundCaptureWindow = new Stage();
        backgroundCaptureWindow.initStyle(StageStyle.TRANSPARENT);
        backgroundCaptureWindow.setAlwaysOnTop(true);

        Scene scene = new Scene(root, 5760, 1080);
        scene.setCursor(Cursor.CROSSHAIR);

        backgroundCaptureWindow.setScene(scene);
        backgroundCaptureWindow.setResizable(false);
        backgroundCaptureWindow.setX(-1920);
        backgroundCaptureWindow.setY(0);
        backgroundCaptureWindow.setOpacity(0.15f);

        backgroundCaptureWindow.show();
    }

    private static void saveFile(File file){
        if(save){
            logger.info("Saving file : " + file.getName());

            try{
                Files.copy(file.toPath(), new File(Main.CAPTURE_PATH + file.getName()).toPath());
                logger.info("  File saved");
            }catch(IOException e){
                logger.error(e);
            }
        }else{
            logger.debug("File save : disable");
        }
    }

    private static void uploadFile(File file){
        if(upload){
            logger.info("Uploading file : " + file.getName());
            Platform.runLater(() -> StatusModel.setStatusMessage("Envoie de l'image en cours..."));

            try{
                ApiUploadXml uploadXml = ApiRequest.uploadFile(file);

                if(uploadXml != null && !uploadXml.hasError()){
                    Platform.runLater(() -> StatusModel.addUploadLink(uploadXml));
                    Platform.runLater(() -> StatusModel.setStatusMessage("Envoie de l'image terminé"));
                    logger.info("  Uploading success");
                }else{
                    logger.error("  Error for uploading file");
                }
            }catch(IOException e){
                logger.error(e);
                Platform.runLater(() -> StatusModel.setStatusMessage("Une erreur est survenu lors de l'envoie de l'image"));
            }catch(URISyntaxException e){
                e.printStackTrace();
            }
        }else{
            logger.debug("File upload : disabled");
        }
    }


    private static void removeTmpFile(File file){
        if(file.delete())
            logger.info("File remove : OK");
        else
            logger.error("File remove : Error");
    }

    private static void clearInterface(GraphicsContext gc, double x, double y, double width, double height){
        gc.clearRect(0, 0, 5760, 1080);
        gc.setFill(Color.BLACK);
        gc.setGlobalAlpha(1);

        if(width == 0 || height == 0){
            gc.fillRect(0, 0, 5760, 1080);
        }else{
            gc.fillRect(0, 0, x, 1080);
            gc.fillRect(x + width, 0, 5760 - (x + width), 1080);
            gc.fillRect(x, 0, width, y);
            gc.fillRect(x, y + height, width, 1080 - (y + height));
        }
    }

    /**
     * Clean interface
     * @param gc GraphicsContext
     */
    private static void clearInterface(GraphicsContext gc){
        clearInterface(gc, 0, 0, 0, 0);
    }

    /**
     * Get X of selection
     * @return double
     */
    private static double getX(){
        if(originMouseX < mouseX)
            return originMouseX;
        else
            return mouseX;
    }

    /**
     * Get Y of selection
     * @return double
     */
    private static double getY(){
        if(originMouseY < mouseY)
            return originMouseY;
        else
            return mouseY;
    }

    /**
     * Get width of selection
     * @return double
     */
    private static double getWidth(){
        if(originMouseX < mouseX)
            return mouseX - originMouseX;
        else
            return originMouseX - mouseX;
    }

    /**
     * Get height of selection
     * @return double
     */
    private static double getHeight(){
        if(originMouseY < mouseY)
            return mouseY - originMouseY;
        else
            return originMouseY - mouseY;
    }
}
