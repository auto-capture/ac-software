package com.winxaito.autocapture.model.network;

import com.winxaito.autocapture.controllers.capture.CaptureController;
import javafx.application.Platform;
import javafx.scene.control.Alert;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class ApiError{
    private int code;
    private String message;

    public ApiError(int code, String message){
        this.code = code;
        this.message = message;
    }

    public ApiError(String code, String message){
        this(Integer.parseInt(code), message);
    }

    public int getCode(){
        return code;
    }

    public void setCode(int code){
        this.code = code;
    }

    public String getMessage(){
        return message;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public static void show(ApiError error){
        Platform.runLater(() -> {
            CaptureController.getWindow().setAlwaysOnTop(false);
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Une erreur est survenu");
            a.setHeaderText("Code d'erreur: " + error.getCode());
            a.setContentText("Message: " + error.getMessage());
            a.showAndWait();
            CaptureController.getWindow().setAlwaysOnTop(true);
        });
    }
}
