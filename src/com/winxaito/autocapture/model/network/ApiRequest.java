package com.winxaito.autocapture.model.network;

import com.winxaito.autocapture.Main;
import com.winxaito.autocapture.model.utils.SettingsModel;
import javafx.util.Pair;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HostnameVerifier;
import java.io.*;
import java.net.*;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class ApiRequest{
    private static Logger logger = LogManager.getLogger();
    public static final String API_URL = "http://api.winxaito.com/";

    private static HttpClient client;
    private static String cookies;
    private static HttpClientContext context;

    private URLConnection connection;

    public static ApiUploadXml loginOfficialServer(String username, String password) throws IOException{
        logger.info("Starting request (Login) (Official server)");

        if(username.equals("") || password.equals("")){
            logger.error("Internal error, username or password is emtpy");
            return new ApiUploadXml(new ApiError(0, "Internal error"));
        }

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(Main.URL_DOMAIN_API + "/auth?output=xml");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        String boundary = "---------------"+ UUID.randomUUID().toString();
        builder.setBoundary(boundary);
        httpPost.addHeader("Content-Type", ContentType.MULTIPART_FORM_DATA.getMimeType()+";boundary="+boundary);
        builder.addTextBody("username", username);
        builder.addTextBody("password", password);

        httpPost.setEntity(builder.build());

        //Response
        HttpResponse response = httpClient.execute(httpPost);
        logger.debug("Response: " + response);
        HttpEntity resEntity = response.getEntity();

        String ret = EntityUtils.toString(resEntity);
        logger.debug("Response content: " + ret);

        EntityUtils.consume(resEntity);
        httpClient.getConnectionManager().shutdown();


        ApiUploadXml apiUploadXml = new ApiUploadXml(ret);

        logger.info("Request termined");
        return apiUploadXml;
    }

    public static ApiUploadXml loginUnofficialServer(String username, String password, String protocol, String domain, int port){
        logger.info("Starting request (Login) (Unofficial server)");

        if(username.equals("") || password.equals("")){
            logger.error("Internal error, key is emtpy");
            return new ApiUploadXml(new ApiError(0, "Internal error"));
        }

        /*DefaultHttpClient httpClient = new DefaultHttpClient();
        SSLSocketFactory sf = new SSLSocketFactory(
                SSLContext.getInstance("TLS"),
                SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        Scheme sch = new Scheme("https", 443, sf);
        httpClient.getConnectionManager().getSchemeRegistry().register(sch);

        //java.net.URI uri = new java.net.URI(protocol, null, domain, port, "/api/auth", "", "");
        HttpPost httpPost = new HttpPost("https://pic.winxaito.com/api/auth");
        httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0");

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        String boundary = "---------------"+ UUID.randomUUID().toString();
        builder.setBoundary(boundary);
        httpPost.addHeader("Content-Type", ContentType.MULTIPART_FORM_DATA.getMimeType()+";boundary="+boundary);
        builder.addTextBody("username", username);
        builder.addTextBody("password", password);

        httpPost.setEntity(builder.build());

        //Response
        HttpResponse response = httpClient.execute(httpPost);
        logger.debug("Response: " + response);
        HttpEntity resEntity = response.getEntity();

        String ret = EntityUtils.toString(resEntity);
        logger.debug("Response content: " + ret);

        EntityUtils.consume(resEntity);
        httpClient.getConnectionManager().shutdown();*/

        /*String urlParameters  = "username=" + username + "&password=" + password;
        byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
        int    postDataLength = postData.length;
        String request        = "https://pic.winxaito.com/api/auth";
        URL    url            = new URL( request );
        HttpURLConnection conn= (HttpURLConnection) url.openConnection();
        conn.setDoOutput( true );
        conn.setInstanceFollowRedirects( false );
        conn.setRequestMethod( "POST" );
        conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty( "charset", "utf-8");
        conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
        conn.setUseCaches( false );
        try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
            wr.write( postData );
        }

        System.out.println(Arrays.toString(postData));*/

        URI uri;
        try{
            uri = new URI(protocol, null, domain, port, "/api/auth", "", "");
        }catch(URISyntaxException e){
            logger.error(e);
            return null;
        }

        URL url;
        try{
            url = new URL(uri.toString());
        }catch(MalformedURLException e){
            logger.error(e);
            return null;
        }
        Map<String,Object> params = new LinkedHashMap<>();
        params.put("username", username);
        params.put("password", password);

        byte[] postDataBytes;
        try{
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet()) {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));

                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            postDataBytes = postData.toString().getBytes("UTF-8");
        }catch(UnsupportedEncodingException e){
            logger.error(e);
            return null;
        }

        HttpURLConnection conn;
        try{
            conn = (HttpURLConnection)url.openConnection();
        }catch(IOException e){
            logger.error(e);
            return null;
        }

        try{
            conn.setRequestMethod("POST");
        }catch(ProtocolException e){
            logger.error(e);
            return null;
        }

        StringBuilder output;
        try{
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            output = new StringBuilder();

            for (int c; (c = in.read()) >= 0;){
                System.out.print((char)c);
                output.append((char)c);
            }
        }catch(IOException e){
            logger.error(e);
            return null;
        }

        ApiUploadXml apiUploadXml = new ApiUploadXml(output.toString());

        logger.info("Request termined");
        return apiUploadXml;
    }

    public static boolean loginZds(String username, String password) throws IOException{
        //TODO: ZdS is aborted for Beta version, waiting Gallery API
        return false;
    }

    public static ApiUploadXml uploadFile(File file) throws IOException, URISyntaxException{
        logger.info("Starting request (Upload file) : " + file.getName());

        initContext();

        FileBody fileBody = new FileBody(file);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addPart("username", new StringBody(Main.settings.getServer_username(), ContentType.MULTIPART_FORM_DATA));
        builder.addPart("password", new StringBody(Main.settings.getServer_password(), ContentType.MULTIPART_FORM_DATA));
        builder.addPart("file", fileBody);

        URI uri;
        try{
            if(Main.settings.getServer_type() == SettingsModel.ServerType.UNOFFICIAL){
                uri = new URI(Main.settings.getServer_unofficial_protocol(), null, Main.settings.getServer_unofficial_host(),
                        Main.settings.getServer_unofficial_port(), "/api/upload", "", "");
            }else{
                uri = new URI("https", null, "pic.winxaito.com",
                        443, "/api/upload", "", "");
            }
        }catch(URISyntaxException e){
            logger.error(e);
            return null;
        }

        Pair<Integer, String> resultPost = sendPost(uri, builder.build());
        System.out.println("Code result: " + resultPost.getKey());
        System.out.println(resultPost.getValue());

        ApiUploadXml apiUploadXml = new ApiUploadXml(resultPost.getValue());

        if(apiUploadXml.hasError()){
            logger.error("apiUploadXml has error, code: " + apiUploadXml.getError().getCode() +
                    " / Message: " + apiUploadXml.getError().getMessage());
            ApiError.show(apiUploadXml.getError());
        }

        logger.info("Request termined");
        return apiUploadXml;
    }

    private static Pair<Integer, String> sendPost(URI uri, HttpEntity entity) {
        HttpPost post = new HttpPost(uri);
        try {
            // add header
            post.setHeader("Host", "pic.winxaito.com");
            post.setHeader("User-Agent", "Mozilla/5.0");
            post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            post.setHeader("Accept-Language", "fr-FR");
            post.setHeader("Connection", "keep-alive");
            post.setHeader("Referer", uri.toString());

            post.setEntity(entity);
            HttpResponse response = client.execute(post, context);

            int responseCode = response.getStatusLine().getStatusCode();
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            return new Pair<>(responseCode, rd.lines().collect(Collectors.joining("\n")));
        } catch (IOException e) {
            logger.error(e);
        }

        return new Pair<>(500, null);
    }

    private static void initContext(){
        context = HttpClientContext.create();

        SSLContextBuilder builder = new SSLContextBuilder();
        SSLConnectionSocketFactory sslsf;
        try{
            builder.loadTrustMaterial(null, (TrustStrategy)(chain, authType) -> true);
            sslsf = new SSLConnectionSocketFactory(builder.build());

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                    .<ConnectionSocketFactory>create()
                    .register("https", sslsf)
                    .register("http", new PlainConnectionSocketFactory())
                    .build();

            PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            cm.setMaxTotal(500);
            cm.setDefaultMaxPerRoute(20);

            client = HttpClients.custom()
                    .setRedirectStrategy(new LaxRedirectStrategy())
                    .setConnectionManager(cm)
                    .setSSLSocketFactory(sslsf)
                    .build();
        }catch(NoSuchAlgorithmException | KeyManagementException | KeyStoreException e){
            logger.error(e);
        }
    }
}
