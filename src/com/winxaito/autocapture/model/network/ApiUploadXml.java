package com.winxaito.autocapture.model.network;

import com.sun.xml.internal.bind.v2.runtime.output.UTF8XmlOutput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class ApiUploadXml{
    private static Logger logger = LogManager.getLogger();

    private Element root;
    private ApiError error;

    public ApiUploadXml(String xml){
        SAXBuilder sxb = new SAXBuilder();

        try{
            Document doc = sxb.build(new ByteArrayInputStream(xml.getBytes("UTF-8")));
            root = doc.getRootElement();

            if(root.getChild("status").getValue().equalsIgnoreCase("error")){
                logger.error("Error in API return, Code: " + root.getChild("code").getValue() +
                        " / Message: " + root.getChild("message").getValue());

                error = new ApiError(
                        root.getChild("code").getValue(),
                        root.getChild("message").getValue()
                );
            }
        }catch(JDOMException | IOException e){
            logger.error("Api communication error", e);
            error = new ApiError(0, "Une erreur est survenu lors de la communication avec l'API");
        }
    }

    public ApiUploadXml(ApiError error){
        this.error = error;
    }

    public boolean hasError(){
        return error != null;
    }

    public ApiError getError(){
        return error;
    }

    public String getUploadLink(){
        if(!hasError())
            return root.getChild("file").getChild("display_link").getValue();

        return "";
    }

    public String getUploadName(){
        if(!hasError())
            return root.getChild("file").getChild("uniqid").getValue();

        return "";
    }
}
