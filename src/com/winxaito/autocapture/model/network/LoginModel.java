package com.winxaito.autocapture.model.network;

import com.winxaito.autocapture.Main;
import com.winxaito.autocapture.model.utils.SettingsModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class LoginModel{
    private static Logger logger = LogManager.getLogger();

    public static ApiUploadXml login(String username, String password, String protocol, String domain, int port){
        ApiUploadXml uploadXml = null;
        logger.debug("Server type: " + Main.settings.getServer_type());

        try{
            if(Main.settings.getServer_type() == SettingsModel.ServerType.OFFICIAL){
                uploadXml = ApiRequest.loginOfficialServer(username, password);
                logger.debug("ApiRequest.loginOfficialServer");
            }else if(Main.settings.getServer_type() == SettingsModel.ServerType.UNOFFICIAL){
                uploadXml = ApiRequest.loginUnofficialServer(username, password, protocol, domain, port);
                logger.debug("ApiRequest.loginUnofficialServer");
            }else{ //TODO: Support for ZDS
                uploadXml = ApiRequest.loginOfficialServer(username, password);
                logger.debug("ApiRequest.loginOfficialServer (By ZDS Login)");
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return uploadXml;
    }
}
