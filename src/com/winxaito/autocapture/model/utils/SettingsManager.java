package com.winxaito.autocapture.model.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Properties;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class SettingsManager{
    private static Logger logger = LogManager.getLogger();

    private static SettingsModel settingsModel;

    private static Properties properties = new Properties();
    private static File configFile;
    private static OutputStream output;
    private static InputStream input;

    public static SettingsModel reloadSettings(){
        settingsModel = null;
        return loadSettings();
    }

    public static SettingsModel loadSettings(){
        if(settingsModel != null)
            return settingsModel;

        loadFile();

        try{
            properties.load(input);
            checkProperties();

            SettingsManager.settingsModel = new SettingsModel(
                    properties.getProperty(SettingsModel.Data.GENERAL_LANG.configKey),
                    properties.getProperty(SettingsModel.Data.GENERAL_THEME.configKey),
                    properties.getProperty(SettingsModel.Data.SERVER_USERNAME.configKey),
                    properties.getProperty(SettingsModel.Data.SERVER_PASSWORD.configKey),
                    properties.getProperty(SettingsModel.Data.SERVER_TYPE.configKey),
                    properties.getProperty(SettingsModel.Data.SERVER_UNOFFICIAL_PROTOCOL.configKey),
                    properties.getProperty(SettingsModel.Data.SERVER_UNOFFICIAL_HOST.configKey),
                    properties.getProperty(SettingsModel.Data.SERVER_UNOFFICIAL_PORT.configKey),
                    properties.getProperty(SettingsModel.Data.SERVER_ZDS_GALLERY.configKey)
            );

            update(settingsModel);

            return SettingsManager.settingsModel;
        }catch(IOException e){
            logger.error("Fatal error: Error for load settings", e);
            System.exit(-1);
        }

        return null;
    }

    public static void update(SettingsModel settingsModel){
        SettingsManager.settingsModel = settingsModel;

        properties.setProperty(SettingsModel.Data.GENERAL_LANG.configKey, settingsModel.getGeneral_lang().configValue);
        properties.setProperty(SettingsModel.Data.GENERAL_THEME.configKey, settingsModel.getGeneral_theme().configValue);
        properties.setProperty(SettingsModel.Data.SERVER_USERNAME.configKey, settingsModel.getServer_username());
        properties.setProperty(SettingsModel.Data.SERVER_PASSWORD.configKey, settingsModel.getServer_password());
        properties.setProperty(SettingsModel.Data.SERVER_TYPE.configKey, settingsModel.getServer_type().configValue);
        properties.setProperty(SettingsModel.Data.SERVER_UNOFFICIAL_PROTOCOL.configKey, settingsModel.getServer_unofficial_protocol());
        properties.setProperty(SettingsModel.Data.SERVER_UNOFFICIAL_HOST.configKey, settingsModel.getServer_unofficial_host());
        properties.setProperty(SettingsModel.Data.SERVER_UNOFFICIAL_PORT.configKey, Integer.toString(settingsModel.getServer_unofficial_port()));
        properties.setProperty(SettingsModel.Data.SERVER_ZDS_GALLERY.configKey, settingsModel.getServer_zds_gallery());

        try{
            output = new FileOutputStream(configFile);
            properties.store(output, "Config file for WxPic");
        }catch(IOException e){
            logger.error(e);
        }
    }

    public static String getDataDirectory(){
        String OS = System.getProperty("os.name").toUpperCase();

        if(OS.contains("WIN"))
            return System.getenv("APPDATA");
        else if(OS.contains("MAC") || OS.contains("NUX"))
            return System.getenv("user.home");
        else
            return System.getenv("user.dir");
    }

    private static void loadFile(){
        configFile = new File(getDataDirectory() + "/wxpic/config.cfg");

        if(!configFile.exists())
            createConfigFile();

        try{
            input = new FileInputStream(configFile);
        }catch(FileNotFoundException e){
            logger.error(e);
        }
    }

    private static void createConfigFile(){
        logger.debug("Create config file");

        if(!configFile.exists()){
            logger.debug("File doesn't exist, creating...");
            if(configFile.getParentFile().mkdirs()){
                try{
                    if(configFile.createNewFile())
                        logger.debug("File created");
                    else
                        logger.error("Error for create config file");
                }catch(IOException e){
                    logger.error(e);
                }
            }else{
                logger.error("Failed for create path in APPDATA");
            }
        }else{
            logger.debug("Config file already exist");
        }
    }

    private static void checkProperties(){
        for(SettingsModel.Data data : SettingsModel.Data.values()){
            if(!properties.containsKey(data.configKey)){
                logger.debug("Error, config file not contain key: <" + data.configKey + ">");
                properties.setProperty(data.configKey, data.defaultValue);
                logger.debug("Key added with default value");
            }
        }
    }
}
