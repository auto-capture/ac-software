package com.winxaito.autocapture.model.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class SettingsModel{
    private static Logger logger = LogManager.getLogger();

    //General config
    private Lang general_lang = Lang.FRENCH;
    private Theme general_theme = Theme.LIGHT;

    //Shortcut settings
    private String shortcut_without_lock;
    private String shortcut_with_lock;
    private String shortcut_fullscreen;

    //Server config
    private String server_username;
    private String server_password;
    private ServerType server_type = ServerType.OFFICIAL;
    private String server_unofficial_protocol;
    private String server_unofficial_host;
    private String server_unofficial_port;
    private String server_zds_gallery;

    public enum Lang{
        ENGLISH("English", "en"),
        FRENCH("Français (French)", "fr");

        public String text;
        public String configValue;

        Lang(String text, String configValue){
            this.text = text;
            this.configValue = configValue;
        }
    }

    public enum Theme{
        LIGHT("Light", "light", "/css/theme_light.css"),
        DARK("Dark", "dark", "/css/theme_dark.css");

        public String text;
        public String configValue;
        public String path;

        Theme(String text, String configValue, String path){
            this.text = text;
            this.configValue = configValue;
            this.path = path;
        }
    }

    public enum ServerType{
        OFFICIAL("Officiel", "official"),
        UNOFFICIAL("Non officiel", "unofficial"),
        ZDS("Zestedesavoir", "zds");

        public String text;
        public String configValue;

        ServerType(String text, String configValue){
            this.text = text;
            this.configValue = configValue;
        }
    }

    public enum Data{
        GENERAL_LANG("data.config.lang", Lang.FRENCH.configValue),
        GENERAL_THEME("data.config.theme", Theme.LIGHT.configValue),

        SERVER_USERNAME("data.server.username", ""),
        SERVER_PASSWORD("data.server.password", ""),
        SERVER_TYPE("data.server.type", ServerType.OFFICIAL.configValue),
        SERVER_UNOFFICIAL_PROTOCOL("data.server.unofficial.protocol", ""),
        SERVER_UNOFFICIAL_HOST("data.server.unofficial.host", ""),
        SERVER_UNOFFICIAL_PORT("data.server.unofficial.port", "0"),
        SERVER_ZDS_GALLERY("data.server.zds.gallery", "");

        String configKey;
        String defaultValue;

        Data(String configKey, String defaultValue){
            this.configKey = configKey;
            this.defaultValue = defaultValue;
        }
    }

    public SettingsModel(String general_lang, String general_theme,
                         String server_username, String server_password, String server_type,
                         String server_unofficial_protocol, String server_unofficial_host,
                         String server_unofficial_port, String server_zds_gallery){
        setGeneral_lang(general_lang);
        setGeneral_theme(general_theme);
        this.server_username = server_username;
        this.server_password = server_password;
        setServer_type(server_type);
        this.server_unofficial_protocol = server_unofficial_protocol;
        this.server_unofficial_host = server_unofficial_host;
        this.server_unofficial_port = server_unofficial_port;
        this.server_zds_gallery = server_zds_gallery;
    }

    public SettingsModel(){}


    //General getters/setters

    public Lang getGeneral_lang(){
        for(Lang lang : Lang.values())
            if(general_lang.configValue.equalsIgnoreCase(lang.configValue))
                return lang;

        return Lang.FRENCH;
    }

    public void setGeneral_lang(String lang){
        for(Lang l : Lang.values())
            if(l.text.equalsIgnoreCase(lang))
                this.general_lang = l;
    }

    public void setGeneral_lang(Lang lang){
        this.general_lang = lang;
    }

    public Theme getGeneral_theme(){
        for(Theme theme : Theme.values())
            if(general_theme.configValue.equalsIgnoreCase(theme.configValue))
                return theme;

        return Theme.LIGHT;
    }

    public String getGeneral_theme_path(){
        if(general_theme.path != null)
            return general_theme.path;

        return Theme.LIGHT.path;
    }

    public void setGeneral_theme(String theme){
        for(Theme t : Theme.values())
            if(t.text.equalsIgnoreCase(theme))
                this.general_theme = t;
    }

    public void setGeneral_theme(Theme theme){
        this.general_theme = theme;
    }

    public String getShortcut_without_lock(){
        return shortcut_without_lock;
    }

    public void setShortcut_without_lock(String shortcut_without_lock){
        this.shortcut_without_lock = shortcut_without_lock;
    }

    public String getShortcut_with_lock(){
        return shortcut_with_lock;
    }

    public void setShortcut_with_lock(String shortcut_with_lock){
        this.shortcut_with_lock = shortcut_with_lock;
    }

    public String getShortcut_fullscreen(){
        return shortcut_fullscreen;
    }

    public void setShortcut_fullscreen(String shortcut_fullscreen){
        this.shortcut_fullscreen = shortcut_fullscreen;
    }

    public String getServer_username(){
        return server_username;
    }

    public void setServer_username(String server_username){
        this.server_username = server_username;
    }

    public String getServer_password(){
        return server_password;
    }

    public void setServer_password(String server_password){
        this.server_password = server_password;
    }

    public ServerType getServer_type(){
        for(ServerType type : ServerType.values())
            if(server_type.configValue.equalsIgnoreCase(type.configValue))
                return type;

        return ServerType.OFFICIAL;
    }

    public void setServer_type(String type){
        logger.debug("Set server type: " + type);

        for(ServerType st : ServerType.values())
            if(st.configValue.equalsIgnoreCase(type))
                this.server_type = st;
    }

    public void setServer_type(ServerType type){
        this.server_type = type;
    }

    public String getServer_unofficial_protocol(){
        return server_unofficial_protocol;
    }

    public void setServer_unofficial_protocol(String server_unofficial_protocol){
        this.server_unofficial_protocol = server_unofficial_protocol;
    }

    public String getServer_unofficial_host(){
        return server_unofficial_host;
    }

    public void setServer_unofficial_host(String server_unofficial_host){
        this.server_unofficial_host = server_unofficial_host;
    }

    public int getServer_unofficial_port(){
        return Integer.parseInt(server_unofficial_port);
    }

    public void setServer_unofficial_port(String server_unofficial_port){
        this.server_unofficial_port = server_unofficial_port;
    }

    public String getServer_zds_gallery(){
        return server_zds_gallery;
    }

    public void setServer_zds_gallery(String server_zds_gallery){
        this.server_zds_gallery = server_zds_gallery;
    }
}

