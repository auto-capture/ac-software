package com.winxaito.autocapture.model.utils;

import com.winxaito.autocapture.controllers.capture.CaptureController;
import javafx.application.Platform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class ShortcutListener implements NativeKeyListener{
    private static Logger logger = LogManager.getLogger();
    private static CaptureController captureController;

    private static boolean initialized = false;

    public static void init(){
        logger.debug("Initialize Shortcut listener");

        initialized = true;
        initKeyListener();
    }

    public static void exit(){
        if(initialized){
            logger.debug("Disable Shortcut listener");

            try{
                GlobalScreen.unregisterNativeHook();
            }catch(NativeHookException e){
                logger.error(e);
            }
        }
    }

    private static void initKeyListener(){
        java.util.logging.Logger logger = java.util.logging.Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(java.util.logging.Level.WARNING);
        logger.setUseParentHandlers(false);

        try{
            GlobalScreen.registerNativeHook();
        }catch (NativeHookException e){
            ShortcutListener.logger.error(e);
        }

        GlobalScreen.addNativeKeyListener(new ShortcutListener());
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent e){
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent e){
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent e){
        /*
         * Key: 67 = F9
         *      68 = F10
         *      65 = F7
         */

        if(e.getKeyCode() == 67){
            Platform.runLater(() -> CaptureController.startCapture(false));
        }else if(e.getKeyCode() == 68){
            Platform.runLater(() -> CaptureController.startCapture(true));
        }else if(e.getKeyCode() == 65){
            //Platform.runLater(() -> CaptureController.showWindow());
            //Todo: System tray (Show window)
        }
    }
}
