package com.winxaito.autocapture.model.utils;

import com.winxaito.autocapture.controllers.capture.CaptureController;
import com.winxaito.autocapture.model.network.ApiUploadXml;

/**
 * Created by: WinXaito (Kevin Vuilleumier)
 */
public class StatusModel{
    private static CaptureController captureController;

    public static void init(CaptureController captureController){
        StatusModel.captureController = captureController;
    }

    public static void setStatusMessage(String message){
        captureController.getStatus().setText(message);
    }

    public static void clearStatusMessage(){
        setStatusMessage("Aucun statut");
    }

    public static void addUploadLink(ApiUploadXml uploadXml){
        captureController.addUploadLink(uploadXml.getUploadLink());
    }
}
